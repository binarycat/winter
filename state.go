package winter

import (
	"hash"
	"sort"
	"fmt"
)

type State struct {
	// map of variables.
	// the special variable "P" stores the current node
	V map[string]string
}

func NewState() *State {
	return &State{ V: make(map[string]string) }
}

func (s *State) Clone() *State {
	ns := NewState()
	for k, v := range s.V {
		ns.V[k] = v
	}
	return ns
}

func (s *State) Hash(h hash.Hash) []byte {
	// TODO: is there a way to get the number of pairs in a map?  if so, capacity should be set to that.
	pairs := make([]string, 0)
	for k, v := range s.V {
		pairs = append(pairs, fmt.Sprintf("%s=%s\n", k, v))
	}
	sort.Strings(pairs)
	h.Reset()
	for _, pair := range pairs {
		h.Write([]byte(pair))
	}
	return h.Sum([]byte{})
}
