package main

import (
	"io"
	"os"
	"log"
	"fmt"
	"bufio"
	"strconv"

	"git.envs.net/binarycat/winter"
)

type Session struct {
	Game *winter.Game
	State *winter.State
}

func NewSession(game *winter.Game) *Session {
	s := &Session{ Game: game, State: game.NewState() }
	return s
}

// get the current node based on the current state
func (s *Session) Node() *winter.Node {
	node, err := s.Game.GetNode(s.State)
	if err != nil { log.Fatal("Session.Node(): ", err) }
	return node
}

func (s *Session) Show(w io.Writer) {
	node := s.Node()
	fmt.Fprintf(w, "%s\n\n", node.Body)
	for i, choice := range node.Choices {
		fmt.Fprintf(w, "%d. %s\n", i+1, choice.Name)
	}
}

func (s *Session) Choose(n int) bool {
	node := s.Node()
	if 0 <= n || n < len(node.Choices) {
		node.Choices[n].Script.Run(s.State)
		return true
	} else {
		return false
	}
}

func main() {
	sep := "\n\n"
	if len(os.Args) < 2 {
		log.Fatalf("USAGE: %s FILENAME [SEP]", os.Args[0])
	}
	if len(os.Args) >= 3 {
		sep = os.Args[2]
		if sep == "clear" {
			sep = "\x1b[H\x1b[2J\x1b[3J"
		}
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	gameBytes, err := os.ReadFile(os.Args[1])
	if err != nil { log.Fatal("unable to read file: ", err) }
	game := winter.NewGame(string(gameBytes))
	s := NewSession(game)
	in := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print(sep)
		s.Show(os.Stdout)
	Scan:
		fmt.Print("\n> ")
		if !in.Scan() { break }
		ln := in.Text()
		if ln == "quit" { return }
		n, err := strconv.Atoi(ln)
		if err != nil || !s.Choose(n-1) {
			fmt.Print("enter a number corrosponding to a choice, or the word 'quit'")
			goto Scan
		}
	}
}
