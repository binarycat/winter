package main

import (
	"git.envs.net/binarycat/winter"
)

var game1 = winter.NewGame(`
P=room1
%%% room1
{if .msg}{.msg}{else}you awake in a barren room.{end}

you see:
* a table with a key lying on it
* a heavy wooden door

>>> turn the door handle
{if .door_unlocked}P=hallway{else}msg=you try the door, but it is locked.{end}

{if not .has_key}
>>> grab the key off the table
has_key=1
msg=you pick up the key.
{else}
>>> try to unlock the door with the key
msg={if .door_unlocked}the door is already unlocked{else}the key turns cleanly in the lock.{end}
door_unlocked=1
{end}


%%% hallway
you stand in a hallway.
`)

