package main

import "html/template"

// arguments for node template
type NodeArg struct {
	Body string
	Choices []ChoiceArg
}

type ChoiceArg struct {
	Name, Hash string
}


var templ = template.Must(template.New("node").Delims("{", "}").Parse(`
{.Body}
<hr/>
{range .Choices}
<a href="{.Hash}">{.Name}</a><br/>
{end}
`))
