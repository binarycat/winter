package main

import (
	"net/http"
	"crypto/md5"
	"encoding/base32"
	"fmt"
	"strings"
	"log"
	
	"git.envs.net/binarycat/winter"
)

type Handler struct {
	Game *winter.Game
	States map[string]*winter.State
}

func NewHandler(g *winter.Game) *Handler {
	return &Handler{
		Game: g,
		States: make(map[string]*winter.State),
	}
}

func (h *Handler) HashState(st *winter.State) string {
	// base32 md5 hash with no padding.
	k := base32.StdEncoding.WithPadding(-1).EncodeToString(st.Hash(md5.New()))
	h.States[k] = st
	return k
}

func (h *Handler) GetNode(st *winter.State) (*NodeArg, error) {
	node, err := h.Game.GetNode(st)
	if err != nil { return nil, err }
	choices := make([]ChoiceArg, len(node.Choices))
	for i, choice := range node.Choices {
		ns := st.Clone()
		choice.Script.Run(ns)
		choices[i].Name = choice.Name
		choices[i].Hash = h.HashState(ns)
	}
	return &NodeArg{
		Body: node.Body,
		Choices: choices,
	}, nil	
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	p := r.URL.Path
	if p == "/play" {
		s := h.HashState(h.Game.NewState())
		http.Redirect(w, r, fmt.Sprintf("/play/%s", s), 307)
	} else if strings.HasPrefix(p, "/play/") {
		hashStr := p[6:]
		st := h.States[hashStr]
		if st == nil {
			log.Print("ServeHTTP(): can't find state with hash %s", hashStr)
			http.NotFound(w, r)
			return
		}
		nodeArg, err := h.GetNode(st)
		if err != nil {
			log.Print("ServeHTTP(): can't get node: ", err)
			http.NotFound(w, r)
			return
		}
		w.Header().Set("Content-Type", "text/html")
		templ.ExecuteTemplate(w, "node", nodeArg)
		// TODO: premptivly execute every choice of the node,
		// hashing the result to make hyperlinks for all the choices
	} else {
		http.NotFound(w, r)
	}
}
