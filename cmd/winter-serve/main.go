package main

import (
	"os"
	"net/http"
	"log"
	
	"git.envs.net/binarycat/winter"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("USAGE: %s FILENAME", os.Args[0])
	}

	gameBytes, err := os.ReadFile(os.Args[1])
	if err != nil { log.Fatal("unable to read file: ", err) }
	handler := NewHandler(winter.NewGame(string(gameBytes)))
	log.Print("server starting")
	log.Fatal(http.ListenAndServe(":7777", handler))
}
