package winter

import (
	"text/template"
	"log"
	"errors"
	"strings"
)

type Page struct {
	name string
	raw string
	templ *template.Template
}

func NewPage(name, src string) *Page {
	p := &Page{ name: name, raw: src }
	templ, err := template.New(name).
		Delims("{","}").
		Option("missingkey=default").
		Parse(src)
	if err != nil {
		log.Print("winter.NewPage(): template parse error: ", err)
	} else {
		p.templ = templ
	}
	return p
}

var ErrTemplate = errors.New("page is missing syntacticaly correct template")

func (p *Page) Render(st *State) (*Node, error) {
	if p.templ == nil { return nil, ErrTemplate }
	var bldr strings.Builder
	bldr.Grow(len(p.raw))
	err := p.templ.Execute(&bldr, st.V)
	if err != nil { return nil, err }
	node, err := ParseNode(bldr.String())
	if err != nil { return nil, err }
	return node, nil
}
