package winter

import (
	"strings"
)

type Choice struct {
	Name string
	Script *Script
}

func (c *Choice) Choose(st *State) *State {
	ns := st.Clone()
	c.Script.Run(ns)
	return ns
}


// a node represents a page that has been template-processed and parsed
type Node struct {
	// body text of the node
	Body string
	Choices []Choice
}

func ParseNode(src string) (*Node, error) {
	parts := strings.Split(src, "\n>>>")
	r := &Node{ Body: parts[0] }
	for _, part := range parts[1:] {
		lines := strings.Split(part, "\n")
		scr, err := ParseScript(lines[1:])
		if err != nil { return nil, err }
		r.Choices = append(r.Choices, Choice{
			Name: strings.TrimSpace(lines[0]),
			Script: scr })
	}
	return r, nil
}

