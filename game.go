package winter

import (
	"log"
	"strings"
	"errors"
	"fmt"
)

type Game struct {
	// maps a page id to a page
	Pages map[string]*Page
	// initialization script
	Init *Script
	// TODO: callback function to get a page if it is not found in the map
	// used if pages are to be stored in an external database
}

func NewGame(src string) *Game {
	parts := strings.Split(src, "\n%%%")
	g := &Game{ Pages: make(map[string]*Page) }
	initScript, err := ParseScript(strings.Split(parts[0], "\n"))
	if err != nil {
		log.Print("NewGame(): init script error: ", err)
	}
	g.Init = initScript
	for _, part := range parts[1:] {
		pageParts := strings.SplitN(part, "\n", 2)
		name := strings.TrimSpace(pageParts[0])
		page := ""
		if len(pageParts) >= 2 {
			page = pageParts[1]
		}
		g.Pages[name] = NewPage(name, page)
	}
	return g
}

var ErrNoPage = errors.New("no page found with name")

func (g *Game) GetNode(st *State) (*Node, error) {
	pageName := st.V["P"]
	page := g.Pages[pageName]
	if page == nil {
		return nil, ErrNoPage
	}
	node, err := page.Render(st)
	if err != nil {
		return nil, err
	}
	// auto-follow if there is a single choice with a blank name
	if len(node.Choices) == 1 && node.Choices[0].Name == "" {
		nst := node.Choices[0].Choose(st)
		nnode, err := g.GetNode(nst)
		if err != nil { return nil, err }
		nnode.Body = fmt.Sprintf("%s\n%s", node.Body, nnode.Body)
		return nnode, nil
	}
	
	return node, nil
}

func (g *Game) NewState() *State {
	st := NewState()
	g.Init.Run(st)
	return st
}
