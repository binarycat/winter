package winter

import (
	"errors"
	"strings"
)

type Script struct {
	Body []Stmt
}

func ParseScript(srcLines []string) (*Script, error) {
	body := make([]Stmt, 0, len(srcLines))
	for _, ln := range srcLines {
		stmt, err := ParseStmt(ln)
		if err != nil { return nil, err }
		if stmt != nil {
			body = append(body, stmt)
		}
	}
	return &Script{ Body: body }, nil
}

func (script *Script) Run(state *State) {
	for _, stmt := range script.Body {
		stmt.Execute(state)
	}
}

var ErrBadStmt = errors.New("invalid statement syntax")

// parse a statement.
// returns double nil for empty/comment lines
func ParseStmt(ln string) (Stmt, error) {
	if len(strings.TrimSpace(ln)) == 0 || ln[0] == '#' {
		// no error, but no statement either.
		return nil, nil
	}
	parts := strings.SplitN(ln, "=", 2)
	if len(parts) != 2 {
		return nil, ErrBadStmt
	}
	return &StmtSet{ parts[0], parts[1] }, nil
}

type Stmt interface {
	Execute(*State)
}

type StmtSet struct {
	Key, Value string
}

func (stmt *StmtSet) Execute(state *State) {
	state.V[stmt.Key] = stmt.Value
}

